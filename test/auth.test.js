import chai from "chai";
import chaiHttp from "chai-http";
import server from "../src";  // This is for unit testing.
import { UserModel, Token } from "../src/modules/users/models";
import userDbo from "../src/modules/users/dbos";

process.env.ENVIRONMENT = "test"

const should = chai.should();

// let server = 'http://localhost:3000';  // This is for api end testing (integration testing).

chai.use(chaiHttp);

const SIGNUP_ROUTE = `/user/signup`
const SIGNIN_ROUTE = `/user/signin`
const PROFILE_ROUTE = `/user/me`
const VERIFY_ROUTE = `/user/verify`
const CHANGE_PWD_ROUTE = `/user/change-password`
const FORGOT_PWD_ROUTE = `/user/forgot-password`
const VERIFY_FORGOT_PWD_ROUTE = `/user/verify/forgot-password`
const CHANGE_FORGOT_PWD_ROUTE = `/user/change/forgot-password`

// let validUser = {
//   email: "testing_12_boilerplate@yopmail.com",
//   password: "qwerty",
//   fullName: "some user"
// }

let validUser = {
	"name": "test",
	"email": "laex_test@yopmail.com",
	"password": "qwerty",
	"mobile": "98765432101",
	"address": {
		"line1": "line1",
		"line2": "line2",
		"city": "hyd",
		"state": "ts",
		"country": "IND",
		"pincode": "500081"
	},
	"profilePicture": "http://test.com",
	// "role": "student",
	// "orgId": "5d529279dcdb25114e278272"
}

describe('User', () => {

  describe(`/POST - ${SIGNUP_ROUTE}`, () => {

    before(function (done) {
      UserModel.deleteMany({ email: validUser.email }, (err, resp) => {
        if (err) console.log("Error while deleting user")
        done()
      })
    });

    before(function (done) {
      Token.deleteMany({ email: validUser.email }, (err, resp) => {
        if (err) console.log("Error while deleting user")
        done()
      })
    });


    it('it should return success false with no email key', (done) => {
      let invalidUser = Object.assign({}, validUser)
      invalidUser.email = undefined

      chai.request(server)
        .post(SIGNUP_ROUTE)
        .send(invalidUser)
        .end((err, res) => {
          res.should.have.status(422);
          done();
        });
    });

    it('it should return success false with invalid email', (done) => {
      let invalidUser = Object.assign({}, validUser)
      invalidUser.email = "wrongEmail"

      chai.request(server)
        .post(SIGNUP_ROUTE)
        .send(invalidUser)
        .end((err, res) => {
          res.should.have.status(422);
          done();
        });
    });

    it('it should return success false with no password key', (done) => {
      let invalidUser = Object.assign({}, validUser)
      invalidUser.email = undefined

      chai.request(server)
        .post(SIGNUP_ROUTE)
        .send(invalidUser)
        .end((err, res) => {
          res.should.have.status(422);
          done();
        });
    });

    it('it should return success false with invalid password lengh < 6', (done) => {
      let invalidUser = Object.assign({}, validUser)
      invalidUser.password = ""

      chai.request(server)
        .post(SIGNUP_ROUTE)
        .send(invalidUser)
        .end((err, res) => {
          res.should.have.status(422);
          done();
        });
    });

    it('it should return success false with invalid password lengh > 32', (done) => {
      let invalidUser = Object.assign({}, validUser)
      invalidUser.password = "12345123451234512345123451234512345"

      chai.request(server)
        .post(SIGNUP_ROUTE)
        .send(invalidUser)
        .end((err, res) => {
          res.should.have.status(422);
          done();
        });
    });

    it('it should return success false with no name key', (done) => {
      let invalidUser = Object.assign({}, validUser)
      invalidUser.name = undefined

      chai.request(server)
        .post(SIGNUP_ROUTE)
        .send(invalidUser)
        .end((err, res) => {
          res.should.have.status(422);
          done();
        });
    });

    it('it should return success false with invalid full name lengh < 2', (done) => {
      let invalidUser = Object.assign({}, validUser)
      invalidUser.name = ""

      chai.request(server)
        .post(SIGNUP_ROUTE)
        .send(invalidUser)
        .end((err, res) => {
          res.should.have.status(422);
          done();
        });
    });

    it('it should return success false with invalid password lengh > 50', (done) => {
      let invalidUser = Object.assign({}, validUser)
      invalidUser.name = "testing123testing123testing123testing123testing1231"
      chai.request(server)
        .post(SIGNUP_ROUTE)
        .send(invalidUser)
        .end((err, res) => {
          res.should.have.status(422);
          done();
        });
    });

    it('it should return success true with valid user details signup', (done) => {
      chai.request(server)
        .post(SIGNUP_ROUTE)
        .send(validUser)
        .end((err, res) => {
          res.should.have.status(201);
          done();
        });
    });
  })

  describe(`/PUT - ${VERIFY_ROUTE}`, () => {
    var userToken = ``
    before((done) => {
      userDbo.getTokenDetails({ email: validUser.email, type: `register`, isUsed: false }, (err, resp) => {
        if (err) console.log('Error while getting user details')
        userToken = resp.token
        done()
      })
    })

    it(`it should return success false if signin before verifying email`, (done) => {
      chai.request(server)
        .post(SIGNIN_ROUTE)
        .send(validUser)
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    })

    it(`it should return success true if token is valid`, (done) => {

      chai.request(server)
        .put(`${VERIFY_ROUTE}?token=${userToken}`)
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
    })

    it(`it should return success false if token is alredy verified`, (done) => {
      chai.request(server)
        .put(`${VERIFY_ROUTE}?token=${userToken}`)
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    })
  })

  describe(`/POST - ${SIGNIN_ROUTE}`, () => {
    it('it should return success false with no email key', (done) => {
      let invalidUser = Object.assign({}, validUser)
      invalidUser.email = undefined

      chai.request(server)
        .post(SIGNUP_ROUTE)
        .send(invalidUser)
        .end((err, res) => {
          res.should.have.status(422);
          done();
        });
    });

    it('it should return success false with invalid email', (done) => {
      let invalidUser = Object.assign({}, validUser)
      invalidUser.email = "wrongEmail"

      chai.request(server)
        .post(SIGNUP_ROUTE)
        .send(invalidUser)
        .end((err, res) => {
          res.should.have.status(422);
          done();
        });
    });

    it('it should return success false with wrong email signin', (done) => {
      let invalidUser = Object.assign({}, validUser)
      invalidUser.email = "email@email.com"

      chai.request(server)
        .post(SIGNIN_ROUTE)
        .send(invalidUser)
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });

    it('it should return success false with no password key', (done) => {
      let invalidUser = Object.assign({}, validUser)
      invalidUser.email = undefined

      chai.request(server)
        .post(SIGNUP_ROUTE)
        .send(invalidUser)
        .end((err, res) => {
          res.should.have.status(422);
          done();
        });
    });

    it('it should return success false with invalid password lengh < 6', (done) => {
      let invalidUser = Object.assign({}, validUser)
      invalidUser.password = ""

      chai.request(server)
        .post(SIGNUP_ROUTE)
        .send(invalidUser)
        .end((err, res) => {
          res.should.have.status(422);
          done();
        });
    });

    it('it should return success false with invalid password lengh > 32', (done) => {
      let invalidUser = Object.assign({}, validUser)
      invalidUser.password = "12345123451234512345123451234512345"

      chai.request(server)
        .post(SIGNUP_ROUTE)
        .send(invalidUser)
        .end((err, res) => {
          res.should.have.status(422);
          done();
        });
    });

    it('it should return success false with wrong password signin', (done) => {
      let invalidUser = Object.assign({}, validUser)
      invalidUser.password = "wrongPassword"

      chai.request(server)
        .post(SIGNIN_ROUTE)
        .send(invalidUser)
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });

    it('it should return success true with valid user signin', (done) => {
      chai.request(server)
        .post(SIGNIN_ROUTE)
        .send(validUser)
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
    });
  })

  describe(`/GET - ${PROFILE_ROUTE}`, () => {

    // after(function () {
    //   UserModel.deleteMany({ email: validUser.email }, (err, resp) => {
    //     if (err) console.log("Error while deleting user")
    //   })
    // });

    var token = ``
    before((done) => {
      chai.request(server)
        .post(SIGNIN_ROUTE)
        .send(validUser)
        .end((err, res) => {
          token = res.body['accessToken']
          res.should.have.status(200);
          done()
        });
    })

    it(`it should return success false with invalid token`, (done) => {
      chai.request(server)
        .get(PROFILE_ROUTE)
        .set('Authorization', "wrongToken")
        .end((err, res) => {
          res.should.have.status(409);
          done()
        });
    })

    it(`it should return success true with valid token`, (done) => {
      chai.request(server)
        .get(PROFILE_ROUTE)
        .set('Authorization', token)
        .end((err, res) => {
          res.should.have.status(200);
          done()
        });
    })
  })

  describe(`/PUT - ${CHANGE_PWD_ROUTE}`, () => {
    var body = {
      oldPassword: `qwerty`,
      newPassword: `qwerty@1`
    }

    var token = ``
    before((done) => {
      chai.request(server)
        .post(SIGNIN_ROUTE)
        .send(validUser)
        .end((err, res) => {
          token = res.body['accessToken']
          res.should.have.status(200);
          done()
        });
    })

    it(`it should return success false with invalid token`, (done) => {
      chai.request(server)
        .put(CHANGE_PWD_ROUTE)
        .set('Authorization', "wrongToken")
        .end((err, res) => {
          res.should.have.status(409);
          done()
        });
    })

    it(`it should return success false with no old pwd key`, (done) => {
      let invalidBody = Object.assign({}, body)
      invalidBody.oldPassword = undefined

      chai.request(server)
        .put(CHANGE_PWD_ROUTE)
        .set('Authorization', token)
        .send(invalidBody)
        .end((err, res) => {
          res.should.have.status(422);
          done()
        });
    })

    it(`it should return success false with no old pwd key`, (done) => {
      let invalidBody = Object.assign({}, body)
      invalidBody.newPassword = undefined

      chai.request(server)
        .put(CHANGE_PWD_ROUTE)
        .set('Authorization', token)
        .send(invalidBody)
        .end((err, res) => {
          res.should.have.status(422);
          done()
        });
    })

    it(`it should return success false with old pwd < 6`, (done) => {
      let invalidBody = Object.assign({}, body)
      invalidBody.oldPassword = "12345"

      chai.request(server)
        .put(CHANGE_PWD_ROUTE)
        .set('Authorization', token)
        .send(invalidBody)
        .end((err, res) => {
          res.should.have.status(422);
          done()
        });
    })

    it(`it should return success false with old pwd > 32`, (done) => {
      let invalidBody = Object.assign({}, body)
      invalidBody.oldPassword = "0123456789012345678901234567890123"

      chai.request(server)
        .put(CHANGE_PWD_ROUTE)
        .set('Authorization', token)
        .send(invalidBody)
        .end((err, res) => {
          res.should.have.status(422);
          done()
        });
    })

    it(`it should return success false with new pwd < 6`, (done) => {
      let invalidBody = Object.assign({}, body)
      invalidBody.newPassword = "12345"

      chai.request(server)
        .put(CHANGE_PWD_ROUTE)
        .set('Authorization', token)
        .send(invalidBody)
        .end((err, res) => {
          res.should.have.status(422);
          done()
        });
    })

    it(`it should return success false with new pwd > 32`, (done) => {
      let invalidBody = Object.assign({}, body)
      invalidBody.newPassword = "0123456789012345678901234567890123"

      chai.request(server)
        .put(CHANGE_PWD_ROUTE)
        .set('Authorization', token)
        .send(invalidBody)
        .end((err, res) => {
          res.should.have.status(422);
          done()
        });
    })

    it(`it should return success false with wrong old pwd `, (done) => {
      let invalidBody = Object.assign({}, body)
      invalidBody.oldPassword = "wrongOldPwd"

      chai.request(server)
        .put(CHANGE_PWD_ROUTE)
        .set('Authorization', token)
        .send(invalidBody)
        .end((err, res) => {
          res.should.have.status(400);
          done()
        });
    })

    it(`it should return success true valid body `, (done) => {
      chai.request(server)
        .put(CHANGE_PWD_ROUTE)
        .set('Authorization', token)
        .send(body)
        .end((err, res) => {
          res.should.have.status(200);
          done()
        });
    })

    it(`it should return success false with old pwd login `, (done) => {
      chai.request(server)
        .post(SIGNIN_ROUTE)
        .send(validUser)
        .end((err, res) => {
          res.should.have.status(400);
          done()
        });
    })

    it(`it should return success true with new pwd login `, (done) => {
      let invalidUser = Object.assign({}, validUser)
      invalidUser.password = body.newPassword

      chai.request(server)
        .post(SIGNIN_ROUTE)
        .send(invalidUser)
        .end((err, res) => {
          res.should.have.status(200);
          done()
        });
    })
  })

  describe(`/POST - ${FORGOT_PWD_ROUTE}`, () => {
    var body = {
      email: validUser.email,
    }

    it(`it should return success false with no email key`, (done) => {
      chai.request(server)
        .post(FORGOT_PWD_ROUTE)
        .end((err, res) => {
          res.should.have.status(422)
          done()
        })
    })

    it(`it should return success false with non existed mail`, (done) => {
      let invalidBody = Object.assign({}, body)
      invalidBody.email = "something@yopmail.com"

      chai.request(server)
        .post(FORGOT_PWD_ROUTE)
        .send(invalidBody)
        .end((err, res) => {
          res.should.have.status(400)
          done()
        })
    })

    it(`it should return success true with existed email`, (done) => {
      chai.request(server)
        .post(FORGOT_PWD_ROUTE)
        .send(body)
        .end((err, res) => {
          res.should.have.status(200)
          done()
        })
    })
  })

  describe(`/GET ${VERIFY_FORGOT_PWD_ROUTE}`, () => {
    var forgotPasswordToken = ``
    before((done) => {
      userDbo.getTokenDetails({ email: validUser.email, type: `forgot`, isUsed: false }, (err, resp) => {
        if (err) console.log('Error while getting user details')
        forgotPasswordToken = resp.token
        done()
      })
    })

    it(`it should return success false with wrong forgot password token`, (done) => {
      chai.request(server)
        .get(`${VERIFY_FORGOT_PWD_ROUTE}?token=wrongToken`)
        .end((err, res) => {
          res.should.have.status(400)
          done()
        })
    })

    it(`it should return success true with correct forgot password token`, (done) => {
      chai.request(server)
        .get(`${VERIFY_FORGOT_PWD_ROUTE}?token=${forgotPasswordToken}`)
        .end((err, res) => {
          res.should.have.status(200)
          done()
        })
    })
  })

  describe(`/PUT ${CHANGE_FORGOT_PWD_ROUTE}`, () => {
    var forgotPasswordToken = ``
    var body = {
      newPassword: validUser.password
    }

    after(function (done) {
      UserModel.deleteMany({ email: validUser.email }, (err, resp) => {
        if (err) console.log("Error while deleting user")
        done()
      })
    });

    after(function (done) {
      Token.deleteMany({ email: validUser.email }, (err, resp) => {
        if (err) console.log("Error while deleting user")
        done()
      })
    });

    before((done) => {
      userDbo.getTokenDetails({ email: validUser.email, type: `forgot`, isUsed: false }, (err, resp) => {
        if (err) console.log('Error while getting user details')
        forgotPasswordToken = resp.token
        done()
      })
    })

    it(`it should return success false with wrong forgot password token`, (done) => {
      chai.request(server)
        .put(`${CHANGE_FORGOT_PWD_ROUTE}?token=wrongToken`)
        .send(body)
        .end((err, res) => {
          res.should.have.status(400)
          done()
        })
    })

    it('it should return success false with no password key', (done) => {
      chai.request(server)
        .put(`${CHANGE_FORGOT_PWD_ROUTE}?token=${forgotPasswordToken}`)
        .send({})
        .end((err, res) => {
          res.should.have.status(422);
          done();
        });
    });

    it('it should return success false with password length < 6', (done) => {
      let invalidBody = Object.assign({}, body)
      invalidBody.newPassword = ""

      chai.request(server)
        .put(`${CHANGE_FORGOT_PWD_ROUTE}?token=${forgotPasswordToken}`)
        .send(invalidBody)
        .end((err, res) => {
          res.should.have.status(422);
          done();
        });
    });

    it('it should return success false with password length > 32', (done) => {
      let invalidBody = Object.assign({}, body)
      invalidBody.newPassword = "01234567890123456789012345678912345"

      chai.request(server)
        .put(`${CHANGE_FORGOT_PWD_ROUTE}?token=${forgotPasswordToken}`)
        .send(invalidBody)
        .end((err, res) => {
          res.should.have.status(422);
          done();
        });
    });

    it(`it should return success true with correct forgot password token`, (done) => {
      chai.request(server)
        .put(`${CHANGE_FORGOT_PWD_ROUTE}?token=${forgotPasswordToken}`)
        .send(body)
        .end((err, res) => {
          res.should.have.status(200)
          done()
        })
    })
  })
})