require('dotenv').config()

import app from "./src";
import morgan from 'morgan';
import config from "./src/config/constants";
import winston from "./src/config/winston";

// app.use(morgan('combined'))
app.use(morgan('combined', { stream: winston.stream }));

app.get('/', (req, res) => {
  res.status(200).send({
    status: "UP",
  })
})

console.log('PORT:', config.PORT)
console.log('ENV:', config.ENVIRONMENT)

app.listen(config.PORT);