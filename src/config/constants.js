require('dotenv').config()

const MONGOURL = process.env.MONGOURL
var DBNAME = process.env.DBNAME
const ENVIRONMENT = process.env.ENVIRONMENT
// console.log("process.env.ENVIRONMENT", process.env.ENVIRONMENT)
if (ENVIRONMENT == "test") {
  DBNAME = process.env.TEST_DBNAME
}
const SENDGRID_API_KEY = process.env.SENDGRID_API_KEY // update your sendgrid key in .env file to change the mail
const ADMIN = process.env.ADMIN
const FRONT_END = process.env.FRONT_END
const PORT = process.env.PORT
const ALGORITHM = process.env.ALGORITHM
const PASSWORD = process.env.PASSWORD
const JWTSECRET = process.env.JWTSECRET
const SENDER_NAME = process.env.SENDER_NAME

export default {
  MONGOURL,
  DBNAME,
  SENDGRID_API_KEY,
  ADMIN,
  FRONT_END,
  PORT,
  ALGORITHM,
  PASSWORD,
  JWTSECRET,
  SENDER_NAME,
  ENVIRONMENT
}
