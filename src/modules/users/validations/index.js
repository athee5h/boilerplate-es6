import joi from "joi";
import { validator, regex } from "../../../utils/utils";

const signup = (req, res, next) => {
  let data = req.body
  
  let signupSchema = joi.object().keys({
    name: joi.string().required().min(4).max(20),
    email: joi.string().email().required().min(3).max(50),
    password: joi.string().required().min(6).max(32),
    mobile: joi.string().required().min(10).max(14),
    address: joi.object().keys({
      line1: joi.string().required().min(4).max(20),
      line2: joi.string().required().min(4).max(20),
      city: joi.string().required(),
      state: joi.string().required(),
      country: joi.string().required(),
      pincode: joi.string().regex(regex.alphaNumeric).min(4).max(12),
    }),
    profilePicture: joi.string(),
  }).options({ allowUnknown: true })

  validator(data, signupSchema, (error, resp) => {
    if (error) {
      res.status(422).send({ message: error.message })
    } else {
      next()
    }
  })
}

const signin = (req, res, next) => {
  let data = req.body
  let signinSchema = joi.object().keys({
    email: joi.string().email().required().min(3).max(64),
    password: joi.string().required().min(6).max(32)
  }).options({ allowUnknown: true })

  validator(data, signinSchema, (error, resp) => {
    if (error) {
      res.status(422).send({ message: error.message })
    } else {
      next()
    }
  })
}

const changePassword = (req, res, next) => {
  let data = req.body

  let changePwdSchema = joi.object().keys({
    oldPassword: joi.string().required().min(6).max(32),
    newPassword: joi.string().required().min(6).max(32)
  })

  validator(data, changePwdSchema, (error, resp) => {
    if (error) {
      res.status(422).send({ message: error.message })
    } else {
      next()
    }
  })
}

const forgotPassword = (req, res, next) => {
  let data = req.body

  let forgotPwdSchema = joi.object().keys({
    email: joi.string().email().required().min(3).max(64),
  })

  validator(data, forgotPwdSchema, (error, resp) => {
    if (error) {
      res.status(422).send({ message: error.message })
    } else {
      next()
    }
  })
}

const changeForgotPassword = (req, res, next) => {
  let data = req.body

  let forgotPwdSchema = joi.object().keys({
    newPassword: joi.string().required().min(6).max(32),
  })

  validator(data, forgotPwdSchema, (error, resp) => {
    if (error) {
      res.status(422).send({ message: error.message })
    } else {
      next()
    }
  })
}

export default {
  signup,
  signin,
  changePassword,
  forgotPassword,
  changeForgotPassword
}