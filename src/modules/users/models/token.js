import mongoose from "mongoose";

let Schema = mongoose.Schema

const TokenSchema = new Schema({
  email: {
    type: String,
    required: true
  },
  token: {
    type: String,
    required: true
  },
  type: {
    type:String,
    enum:['forgot', 'register'],
    required: true,
  },
  isUsed: {
    type: Boolean,
    default: false
  }
}, {
    versionKey: false,
    strict: false,
  })

let Token = mongoose.model('token', TokenSchema)

export {
  Token
}