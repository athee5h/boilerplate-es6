import mongoose from "mongoose";

let Schema = mongoose.Schema

const UserSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    unique: true,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  mobile: {
    type: String,
    required: true
  },
  createdAt: {
    type: Number,
    default: Date.now
  },
  updatedAt: {
    type: Number,
    default: Date.now
  },
  status: {
    type: String,
    enum: ["active", "inactive", "suspended"],
    default: "inactive"
  },
  address: {
    line1: {
      type: String,
    },
    line2: {
      type: String,
    },
    city: {
      type: String
    },
    state: {
      type: String
    },
    country: {
      type: String
    },
    pincode: {
      type: Number
    }
  },
  profilePicture: {
    type: String,
  },  
}, {
    versionKey: false,
    strict: false,
  })

let UserModel = mongoose.model('user', UserSchema)

export {
  UserModel
}