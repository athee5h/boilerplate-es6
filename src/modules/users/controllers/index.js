import { waterfall } from "async";
// import { userDbo, userDbo } from "../dbos";
import userDbo from "../dbos";
import { getToken } from "../../../helpers/jwt";
import config from "../../../config/constants";
import helpers from "../../../helpers";

/**
 * @api {post} /user/signup
 * @apiName CreateUser
 * @apiGroup User
 * 
 * @apiParam {String} full_name  Full name of the User.
 * @apiParam {String} email      Email of the User.
 * @apiParam {String} password   Password of the Users.
 * 
 * @apiSuccess {Boolean} success Api response.
 * @apiSuccess {String} message  Success message.
 * 
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true,
 *       "message": "User created successfully"
 *     }
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 400 Validation failed
 *     {
 *       "success": false,
 *       "message": ["Error while creating user" | "Email already registered" | "Error while sending mail" ]
 *     }
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 422 Validation failed
 *     {
 *       "success": false,
 *       "message": "message"
 *     }
 * 
 */

const singup = (req, res) => {
  let data = req.body
  var token = ''
  var userData = null

  waterfall([
    (next) => {
      userDbo.getUser({ email: data.email }, (err, user) => {
        userData = user
        if (err) {
          next({ message: "Error while creating user" })
        } else if (user && user.status !== "inactive") {
          next({ message: "Email already registered" })
        } else {
          next()
        }
      })
    }, (next) => {
      var from = data.email
      var subject = "Welcome"
      token = helpers.encrypt({ email: data.email, generatedTime: Date.now() })
      var link = `${config.FRONT_END}auth/verify?token=${token}`
      var message = "Click the link to board on to our platform " + link
      if (config.ENVIRONMENT == "test") {
        next()
      } else {
        helpers.sendMail(from, subject, message, (err) => {
          if (err) {
            next({ message: "Error while sending mail" })
          } else {
            next()
          }
        })
      }
    }, (next) => {
      data.password = helpers.encodeText(data.password)
      if (!userData) {
        userDbo.createUser(data, (err, resp) => {
          if (err) {
            next({
              message: 'Error in creating user1',
            }, null)
          } else {
            next()
          }
        })
      } else {
        data.updatedAt = Date.now()
        userDbo.updateUser({ email: data.email }, data, (err, resp) => {
          if (err) {
            next({
              message: 'Error in creating user2',
            }, null)
          } else {
            next()
          }
        })
      }
    }, (next) => {
      userDbo.upsertToken({ email: data.email, type: "register" }, { email: data.email, token, type: "register", isUsed: false }, (err, resp) => {
        if (err) {
          next({ message: "Error while storing token details" })
        } else {
          next(null, {
            message: 'User created successfully'
          })
        }
      })
    }], (error, resp) => {
      if (error) res.status(400).send({ success: false, ...error })
      else res.status(201).send({ success: true, ...resp })
    })
}

/**
 * @api {post} /user/signin
 * @apiName LoginUser
 * @apiGroup User
 * 
 * @apiParam {String} email      Email of the User.
 * @apiParam {String} password   Password of the User.
 * 
 * @apiSuccess {Boolean} success Api response.
 * @apiSuccess {String} message  Success message.
 * @apiSuccess {String} accessToken Access token for user.
 * @apiSuccess {String} refreshToken Refresh tokoen for user.
 * 
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true,
 *       "message": "User login successful",
 *       "accessToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmdWxsX25hbWUiOiJhdGhlZXNoIiwiZW1...",
 *       "refreshToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmdWxsX25hbWUiOiJhdGhlZXNoIiwiZW1...",
 *     }
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 400 Bad request
 *     {
 *       "success": false,
 *       "message": ["Error in finding user" | "User not found" | "Please verify your email to login" | "Password not matched"]
 *     } 
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 422 Validation failed
 *     {
 *       "success": false,
 *       "message": "message"
 *     }
 */


const signin = (req, res) => {
  let { email, password } = req.body;

  waterfall([
    (next) => {
      var projectDetails = {
        email: 1,
        status: 1,
        permissions: 1,
        role: 1,
        mobile: 1,
        orgId: 1,
        password: 1
      }
      userDbo.getUserWithProject({ email }, projectDetails, (error, user) => {
        if (error) {
          next({ message: 'Error in finding user' })
        } else if (!user) {
          next({ message: 'User not found' })
        } else {
          next(null, user)
        }
      })
    }, (user, next) => {
      if (helpers.encodeText(password) === user.password) {
        if (user.status === "inactive") {
          next({ message: 'Please verify your email to login' })
        } else if (user.status === "suspended") {
          next({ message: "Your account has been suspended" })
        } else {
          user = user.toObject()
          delete user.password
          user = helpers.encrypt(user)
          let accessToken = getToken(user, 'access')
          let refreshToken = getToken(user, 'refresh')
          let resp = {
            accessToken: accessToken,
            refreshToken: refreshToken
          }
          next(null, resp)
        }
      } else {
        next({ message: 'Password not matched' })
      }
    }], (error, resp) => {
      if (error) res.status(400).send({ success: false, ...error })
      else res.status(200).send({ success: true, ...resp })
    })
}


/**
 * @api {get} /user/profile
 * @apiName UserDetails
 * @apiGroup User
 * 
 * @apiHeader {String} authorization User access token
 * 
 * @apiSuccess {Boolean} success Api response.
 * @apiSuccess {json} data response object
 * 
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true,
 *        "data" : {
 *          _id: "uuid",
 *          full_name: "sample",
 *          email: "email"
 *        }
 *     }
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 409 Unauthorized user
 *     {
 *       "success": false,
 *       "message": "Invalid token"
 *     }
 */

const profile = (req, res) => {
  res.status(200).send({ success: true, data: req.user })
}


/**
 * @api {put} /user/change-password
 * @apiName ChangePassword
 * @apiGroup User
 * 
 * @apiParam {String} oldPassword   Old password of the User.
 * @apiParam {String} newPassword   New password to be changed.
 * 
 * @apiHeader {String} authorization   User access token.
 * 
 * @apiSuccess {Boolean} success Api response.
 * @apiSuccess {String} message  Success message.
 * 
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true,
 *       "message": "Password changed successfully",
 *     }
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 400 Bad request
 *     {
 *       "success": false,
 *       "message": ["Error while finding user" | "Current password not matching" | "Error while changing password"]
 *     } 
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 422 Validation failed
 *     {
 *       "success": false,
 *       "message": "message"
 *     }
 */

const changePassword = (req, res) => {
  var { body, user } = req

  waterfall([
    (next) => {
      userDbo.getUser({ email: user.email }, (err, resp) => {
        if (err) {
          next({ message: "Error while finding user" })
        } else if (resp.password !== helpers.encodeText(body.oldPassword)) {
          next({ message: "Current password not matching" })
        } else {
          next()
        }
      })
    }, (next) => {
      var password = helpers.encodeText(body.newPassword)
      userDbo.updateUser({ email: user.email }, { password }, (err, resp) => {
        if (err) {
          next({ message: "Error while changing password" })
        } else {
          next(null, { message: "Password changed successfully" })
        }
      })
    }], (error, resp) => {
      if (error) res.status(400).send({ success: false, ...error })
      else res.status(200).send({ success: true, ...resp })
    })
}

/**
 * @api {put} /user/verify?token=dfadf....
 * @apiName verifySignupToken
 * @apiGroup User
 * 
 * @apiParam {String} token   token that sent in email.
 * 
 * @apiSuccess {Boolean} success Api response.
 * @apiSuccess {String} message  Success message.
 * 
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true,
 *       "message": "User verified successfully",
 *     }
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 400 Bad request
 *     {
 *       "success": false,
 *       "message": ["Invalid link" | "Error while reading token" | "Error while verifying user" | "User already verified" | "Error while updating user"]
 *     } 
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 422 Validation failed
 *     {
 *       "success": false,
 *       "message": "message"
 *     }
 */

const verifySignupToken = (req, res) => {
  var token = req.query["token"]
  var data = null

  waterfall([
    (next) => {
      if (!token) {
        next({ message: "Invalid link" })
      } else {
        userDbo.getTokenDetails({ token, type: 'register', isUsed: false }, (err, resp) => {
          if (err) {
            next({ message: "Error while reading token" })
          } else if (!resp) {
            next({ message: "Invalid link" })
          } else {
            next()
          }
        })
      }
    }, (next) => {
      try {
        data = helpers.decrypt(token)
        next()
      } catch (error) {
        next({ message: "Error while reading token" })
      }
    }, (next) => {
      userDbo.getUser({ email: data.email }, (err, user) => {
        if (err) {
          next({ message: "Error while verifying user" })
        } else if (user.status === "active") {
          next({ message: "User already verified" })
        } else {
          next()
        }
      })
    }, (next) => {
      if (data && data.email) {
        userDbo.updateUser({ email: data.email }, { status: "active" }, (err, resp) => {
          if (err) {
            next({ message: "Error while updating user" })
          } else {
            userDbo.updateTokenDetails({ email: data.email, token, isUsed: false, type: 'register' }, { isUsed: true })
            next(null, { message: "User verified successfully" })
          }
        })
      } else {
        next({ message: "Error while reading token" })
      }
    }], (error, resp) => {
      if (error) res.status(400).send({ success: false, ...error })
      else res.status(200).send({ success: true, ...resp })
    })
}

/**
 * @api {post} /user/forgot-password
 * @apiName forgotPassword
 * @apiGroup User
 * 
 * @apiParam {String} email User email.
 * 
 * @apiSuccess {Boolean} success Api response.
 * @apiSuccess {String} message Success message.
 * 
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true,
 *       "message": "Verification link sent your email",
 *     }
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 400 Bad request
 *     {
 *       "success": false,
 *       "message": ["Error in finding user" | "User not found" | "Error while sending link"]
 *     } 
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 422 Validation failed
 *     {
 *       "success": false,
 *       "message": "message"
 *     }
 */

const forgotPassword = (req, res) => {
  var { email } = req.body
  var token = ""

  waterfall([
    (next) => {
      userDbo.getUser({ email }, (err, resp) => {
        if (err) {
          next({ message: "Error in finding user" })
        } else if (!resp) {
          next({ message: "User not found" })
        } else {
          try {
            token = helpers.encrypt({ email, generatedTime: Date.now() })
            next()
          } catch (error) {
            next({ message: "Error while generating link" })
          }
        }
      })
    }, (next) => {
      userDbo.getTokenDetails({ token, isUsed: false, type: 'forgot' }, (err, resp) => {
        if (err) {
          next({ message: "Error while sending link" })
        } else if (!resp) {
          userDbo.storeTokenDetails({ email, token, type: 'forgot' }, (err, resp) => {
            if (err) next({ message: "Error while storing link" })
            else next()
          })
        } else {
          userDbo.updateTokenDetails({ token, isUsed: false, type: 'forgot' }, { token }, (err, resp) => {
            if (err) next({ message: "Error while updating token details" })
            else next()
          })
        }
      })
    }, (next) => {
      if (config.ENVIRONMENT == "test") {
        next(null, { message: "Verification link sent your email" })
      } else {
        var from = email
        var subject = "Reset forgot password"
        var link = `${config.FRONT_END}auth/forgot-password/verify?token=${token}`
        var message = "Click the link to set new password " + link
        helpers.sendMail(from, subject, message, (err) => {
          if (err) {
            next({ message: "Error while sending mail" })
          } else {
            next(null, { message: "Verification link sent your email" })
          }
        })
      }
    }], (error, resp) => {
      if (error) res.status(400).send({ success: false, ...error })
      else res.status(200).send({ success: true, ...resp })
    })
}


/**
 * @api {get} /user/verify/forgot-password?token=dfadf....
 * @apiName verifyForgotPassword
 * @apiGroup User
 * 
 * @apiParam {String} token   token that sent in email.
 * 
 * @apiSuccess {Boolean} success Api response.
 * @apiSuccess {String} message  Success message.
 * 
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true,
 *       "message": "Link is available",
 *     }
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 400 Bad request
 *     {
 *       "success": false,
 *       "message": ["Error while verifying link" | "Invalid token"]
 *     } 
 */

const verifyForgotPassword = (req, res) => {
  var query = req.query
  var token = null
  waterfall([
    (next) => {
      if (!query || !query["token"]) {
        next({ message: "Error while verifying link" })
      } else {
        token = query["token"]
        next()
      }
    }, (next) => {
      userDbo.getTokenDetails({ token, type: 'forgot', isUsed: false }, (err, resp) => {
        if (err) next({ message: "Error while fetching token details" })
        else if (!resp) next({ message: "Invalid token" })
        else next()
      })
    }, (next) => {
      // var expireTime = 1 * 60 * 1000 // 1 min
      var expireTime = 30 * 60 * 60 * 1000 // 30 min
      helpers.isLinkExpired(token, expireTime, (err) => {
        if (err) {
          next(err)
        } else {
          next(null, { message: "Link is available" })
        }
      })
    }], (error, resp) => {
      if (error) res.status(400).send({ success: false, ...error })
      else res.status(200).send({ success: true, ...resp })
    })
}

/**
 * @api {put} /change/forgot-password?token=dfadf....
 * @apiName changeForgotPassword
 * @apiGroup User
 * 
 * @apiParam {String} newPassword User email.
 * 
 * @apiSuccess {Boolean} success Api response.
 * @apiSuccess {String} message Success message.
 * 
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true,
 *       "message": "Password updated successfully",
 *     }
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 400 Bad request
 *     {
 *       "success": false,
 *       "message": ["Error while verifying link" | "Invalid token" | "Error while updating password"]
 *     } 
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 422 Validation failed
 *     {
 *       "success": false,
 *       "message": "message"
 *     }
 */

const changeForgotPassword = (req, res) => {
  var query = req.query
  var token = null
  var userData = null
  var body = req.body

  waterfall([
    (next) => {
      if (!query || !query["token"]) {
        next({ message: "Error while verifying link" })
      } else {
        token = query["token"]
        next()
      }
    }, (next) => {
      userDbo.getTokenDetails({ token, isUsed: false, type: 'forgot' }, (err, resp) => {
        if (err) next({ message: "Error while fetching token details" })
        else if (!resp) next({ message: "Invalid token" })
        else next()
      })
    }, (next) => {
      // var expireTime = 1 * 60 * 1000 // 1 min
      var expireTime = 30 * 60 * 60 * 1000 // 30 min
      helpers.isLinkExpired(token, expireTime, (err, data) => {
        if (err) {
          next(err)
        } else {
          userData = data
          next()
        }
      })
    }, (next) => {
      userDbo.updateUser({ email: userData.email }, { password: helpers.encodeText(body.newPassword) }, (err, resp) => {
        if (err) {
          next({ message: "Error while updating password" })
        } else {
          userDbo.updateTokenDetails({ token, isUsed: false, type: 'forgot' }, { isUsed: true }, (err, resp) => {
            if (err) next({ message: "Error while updating token details" })
            else next(null, { message: "Password updated successfully" })
          })
        }
      })
    }], (error, resp) => {
      if (error) res.status(400).send({ success: false, ...error })
      else res.status(200).send({ success: true, ...resp })
    })
}

export default {
  singup,
  signin,
  profile,
  changePassword,
  verifySignupToken,
  forgotPassword,
  verifyForgotPassword,
  changeForgotPassword
}