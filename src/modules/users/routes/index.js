import { Router } from "express";
import userControllers from "../controllers";
import userValidations from "../validations";
import middlewares from "../../../middlewares";

let routes = Router()

routes.post('/signup', userValidations.signup, userControllers.singup)
routes.put ('/verify', userControllers.verifySignupToken)
routes.post('/signin', userValidations.signin, userControllers.signin)
routes.get ('/me', middlewares.isAuthenticated, userControllers.profile)
routes.put ('/change-password', middlewares.isAuthenticated, userValidations.changePassword, userControllers.changePassword)
routes.post('/forgot-password', userValidations.forgotPassword, userControllers.forgotPassword)
routes.get ('/verify/forgot-password', userControllers.verifyForgotPassword)
routes.put ('/change/forgot-password', userValidations.changeForgotPassword, userControllers.changeForgotPassword)

export default routes