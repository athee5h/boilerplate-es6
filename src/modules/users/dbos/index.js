import { UserModel, Token } from "../models";

const createUser = (data, cb) => {
  var user = new UserModel(data)
  user.save(cb)
  // UserModel.updateOne(data, { $set: data }, { upsert: true }, cb)
}

const getUser = (data, cb) => {
  UserModel.findOne(data, cb)
}

const getUserWithProject = (data, project, cb) => {
  UserModel.findOne(data, project, cb)
}

const updateUser = (findQuery, updateQuery, cb) => {
  UserModel.updateOne(findQuery, { $set: updateQuery }, cb)
}

const getTokenDetails = (findQuery, cb) => {
  Token.findOne(findQuery, cb)
}

const updateTokenDetails = (findQuery, updateQuery, cb) => {
  Token.updateOne(findQuery, { '$set': updateQuery }, cb)
}

const storeTokenDetails = (tokenDetails, cb) => {
  var data = new Token(tokenDetails)
  data.save(cb)
}

const upsertToken = (findToken, tokenDetails, cb) => {
  Token.updateOne(findToken, { $set: tokenDetails }, { upsert: true }, cb)
}

export default {
  getTokenDetails,
  updateTokenDetails,
  storeTokenDetails,
  upsertToken,
  createUser,
  getUser,
  getUserWithProject,
  updateUser
}