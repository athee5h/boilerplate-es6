import { verifyToken } from "../helpers/jwt";
import helpers from "../helpers";

const isAuthenticated = (req, res, next) => {
  let token = req.headers.authorization
  if (!token) {
    res.status(409).send({
      success: false,
      message: "Missing authorization token"
    })
    return
  }

  verifyToken(token, (error, resp) => {
    if (error) {
      res.status(409).send(error)
    } else {
      let data = helpers.decrypt(resp.data)
      req.user = data
      next()
    }
  })
}

const isAuthorized = (role) => {
  return (req, res, next) => {
    var user = req.user
    if (user.role != role) {
      res.status(401).send({
        success: false,
        message: "Unauthorized user"
      })
    } else {
      next()
    }
  }
}

export default {
  isAuthenticated,
  isAuthorized
}