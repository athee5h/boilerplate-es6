// require('dotenv').config()

import bodyParser from "body-parser";
import express from "express";
import mongoose from "mongoose";
import cors from "cors";

import config from "./config/constants";
import userRoutes from "./modules/users/routes";
// import orgRoutes from "./modules/organizations/routes";

import morgan from 'morgan';
import winston from "./config/winston";

const uri = `${config.MONGOURL}/${config.DBNAME}`
mongoose.connect(uri, { useNewUrlParser: true }, (error, result) => {
  if (error) {
    console.log('Error in connecting to mongodb')
  } else {
    console.log('Mongo connection made successfully to :', config.DBNAME)
  }
})

var app = express();
app.use(bodyParser.json())
app.use(cors())

// app.use(morgan('combined'))
if (config.ENVIRONMENT !== 'test')
  app.use(morgan('combined', { stream: winston.stream }));

app.use('/user', userRoutes)
// app.use('/organizations', orgRoutes)

export default app