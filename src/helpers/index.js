import crypto from "crypto";
import sgMail from "@sendgrid/mail";
import config from "../config/constants";

var algorithm = config.ALGORITHM
var privateKey = config.PASSWORD

const encodeText = (text) => {
  var encoded = crypto.createHash('sha256').update(text).digest('hex')
  return encoded
}

const decrypt = (token) => {
  var decipher = crypto.createDecipher(algorithm, privateKey);
  var dec = decipher.update(token, 'hex', 'utf8');
  dec += decipher.final('utf8');
  dec = JSON.parse(dec)
  return dec;
}

const encrypt = (data) => {
  data = JSON.stringify(data)
  var cipher = crypto.createCipher(algorithm, privateKey);
  var crypted = cipher.update(data, 'utf8', 'hex');
  crypted += cipher.final('hex');
  return crypted;
}

const sendMail = (to, subject, message, cb) => {
  sgMail.setApiKey(config.SENDGRID_API_KEY);
  var from = config.ADMIN
  const msg = {
    to,
    from,
    subject,
    text: message,
  };

  try {
    sgMail.send(msg);
    cb(null)
  } catch (error) {
    cb(error)
  }
}

const isLinkExpired = (token, expireTime, cb) => {
  var presentTime = Date.now()
  try {
    var data = decrypt(token)
  } catch (error) {
    cb(error, null)
    return
  }

  if (!data && !data.generatedTime) {
    cb({ message: "Error while decoding link" })
  } else if (presentTime - data.generatedTime > expireTime) {
    cb({ message: "Link Expired" })
  }
  cb(null, data)
}

export default {
  encodeText,
  encrypt,
  decrypt,
  sendMail,
  isLinkExpired
}